#!/bin/bash
#https_proxy=http://192.0.2.192:18888
https_proxy=
gvisitorid=
sts=20131
ikey=AIzaSyAO_FJ2SlqU8Q4STEHLGCilw_Y9_11qcW8
iversion=7.20250211.01.00
cipher () { :
json_cipher [["sts",20131],["splice",1],["reverse"],["swap",10],["swap",48],["splice",3]]
}
splice () { :
local re="^[1-9]$"
[[ "$1" =~ $re ]] || return
youtsuck[0]="${youtsuck[0]:$1}"
}
swap () { :
local re="^[1-9][0-9]?$"
[[ "$1" =~ $re ]] || return
local L=${#youtsuck[0]}
local w="$(($1%$L))"
youtsuck[0]="${youtsuck[0]:$w:1}${youtsuck[0]:1:$(($w-1))}${youtsuck[0]:0:1}${youtsuck[0]:$(($w+1))}"
}
reverse () { :
local r=
local L=${#youtsuck[0]}
local i ; for (( i = 0 ; i < L ; ++i )) ; do r="${youtsuck[0]:${i}:1}${r}" ; done
youtsuck[0]="${r}"
}
jwz_cipher () { :
local instructions="$1"
instructions="${instructions%%#*}"
instructions="${instructions//,}"
set -f ; instructions=($instructions)
local operation
for operation in "${instructions[@]}" ; do
case "${operation:0:1}" in
r ) reverse ;;
s ) splice "${operation:1}" ;;
w ) swap "${operation:1}" ;;
esac ; done
}
json_cipher () { :
local instructions="$1"
local jsonre="^\[(\[\"?[a-z0-9]+\"?(,\"?[a-z0-9]+\"?)?\],)+\]$"
[[ "${instructions/%\]\]/\],\]}" =~ $jsonre ]] || return
instructions="${instructions//[\[\"\] ]}"
instructions="${instructions//,/ }"
set -f ; instructions=($instructions)
local N=${#instructions[@]}
local i ; for (( i = 0 ; i < N ; ++i )) ; do
case "${instructions[i]}" in
splice ) splice "${instructions[i+1]}" ;;
swap ) swap "${instructions[i+1]}" ;;
reverse ) reverse ;;
esac ; done
}
idre="[/=]([-0-9a-zA-Z_]{11})([?#])?$"
[[ "$1" =~ $idre ]] && id="${BASH_REMATCH[1]}" || exit
if [[ "${BASH_REMATCH[2]}" != "?" ]] ; then
[[ "${BASH_REMATCH[2]}" != "#" ]] && iscreen="ADUNIT" || iscreen="EMBED"
version=
if [[ ${#https_proxy} -eq 0 && -x lsquic/http_client && $(($RANDOM % 2)) -eq 0 ]] ; then
if (( RANDOM % 2 )) ; then version=h3 ; delayed_acks=0 ; else version=h3-29 ; delayed_acks=0 ; fi ; fi
readarray -n 2 -t youtsuck < <( \
{ dirname_re="(^.*/)[^/]+$" ; [[ "${BASH_SOURCE[0]}" =~ $dirname_re ]] && cd "${BASH_REMATCH[1]}"
if [[ "${version}" =~ ^[hQ] ]] ; then
post="$(mktemp)" && {
echo -n '{"videoId":"'"${id}"'","context":{"client":{"clientName":"TVHTML5","clientVersion":"'"${iversion}"'","clientScreen":"'"${iscreen}"'"},"thirdParty":{"embedUrl":"https://www.youtube.com/"}},"playbackContext":{"contentPlaybackContext":{"signatureTimestamp":'"${sts}"'}}}' > "${post}"
lsquic/http_client -o version=${version} -o delayed_acks=${delayed_acks} -o cc_algo=1 -o pace_packets=0 -0 /tmp/www.youtube.com,${version}.quic -$((RANDOM%2*2+4)) \
-M POST -s www.youtube.com -P "${post}" -p '/youtubei/v1/player?key='"${ikey}"
rm -f "${post}" ; }
else
[[ ${#https_proxy} -ne 0 ]] && p=-4 || p=-$((RANDOM%2*2+4))
https_proxy="${https_proxy}" wget --no-config --no-cookies --no-dns-cache --no-hsts --no-check-certificate --connect-timeout=10 --read-timeout=30 -q -t1 -O- -o /dev/null $p \
--post-data='{"videoId":"'"${id}"'","context":{"client":{"clientName":"TVHTML5","clientVersion":"'"${iversion}"'","clientScreen":"'"${iscreen}"'"},"thirdParty":{"embedUrl":"https://www.youtube.com/"}},"playbackContext":{"contentPlaybackContext":{"signatureTimestamp":'"${sts}"'}}}' \
--header='X-Goog-Visitor-Id: '"${gvisitorid}" \
--header='Content-Type: application/json' 'https://www.youtube.com/youtubei/v1/player?key='"${ikey}"
fi ; } | \
grep -E -s -o '"s=[^ &]+|https://[^. ]+\.googlevideo\.com/videoplayback[^ "]+' )
if [[ "${version}" == "h3" ]] ; then
if [[ ${#youtsuck[0]} -eq 0 ]] ; then
echo -n > /tmp/www.youtube.com,h3.quic ; fi ; fi
if [[ "${youtsuck[0]:0:3}" == "htt" ]] ; then
nre="&n=([A-Za-z0-9_-]+)"
if [[ "${youtsuck[0]}" =~ $nre ]] ; then
( dirname_re="(^.*/)[^/]+$" ; [[ "${BASH_SOURCE[0]}" =~ $dirname_re ]] && cd "${BASH_REMATCH[1]}"
if [[ "${youtsuck[0]}" =~ $nre ]] ; then
if [[ -x ./n-unslow ]] ; then
unslow="$(./n-unslow "${BASH_REMATCH[1]}")"
youtsuck[1]="${BASH_REMATCH[0]}"
unre="^[A-Za-z0-9_-]+$"
if [[ "${unslow}" =~ $unre ]] ; then
youtsuck[0]="${youtsuck[0]/${youtsuck[1]}/&n=${unslow}}"
fi ; fi ; fi
echo "${youtsuck[0]}"
) ; else
echo "${youtsuck[0]}"
fi
exit
else
[[ "${youtsuck[1]:0:3}" == "htt" ]] || exit
youtsuck[0]="${youtsuck[0]:3}"
youtsuck[0]="${youtsuck[0]//%3D/=}"
youtsuck[1]="${youtsuck[1]//%3F/?}"
youtsuck[1]="${youtsuck[1]//%3D/=}"
youtsuck[1]="${youtsuck[1]//%25/%}"
youtsuck[1]="${youtsuck[1]//%26/&}"
cipher
youtsuck[0]="${youtsuck[1]}&sig=${youtsuck[0]}"
nre="&n=([A-Za-z0-9_-]+)"
if [[ "${youtsuck[0]}" =~ $nre ]] ; then
( dirname_re="(^.*/)[^/]+$" ; [[ "${BASH_SOURCE[0]}" =~ $dirname_re ]] && cd "${BASH_REMATCH[1]}"
if [[ "${youtsuck[0]}" =~ $nre ]] ; then
if [[ -x ./n-unslow ]] ; then
unslow="$(./n-unslow "${BASH_REMATCH[1]}")"
youtsuck[1]="${BASH_REMATCH[0]}"
unre="^[A-Za-z0-9_-]+$"
if [[ "${unslow}" =~ $unre ]] ; then
youtsuck[0]="${youtsuck[0]/${youtsuck[1]}/&n=${unslow}}"
fi ; fi ; fi
echo "${youtsuck[0]}"
) ; else
echo "${youtsuck[0]}"
fi
exit
fi
else
version=
if [[ ${#https_proxy} -eq 0 && -x lsquic/http_client && $(($RANDOM % 2)) -eq 0 ]] ; then
if (( RANDOM % 2 )) ; then version=h3 ; delayed_acks=0 ; else version=h3-29 ; delayed_acks=0 ; fi ; fi
readarray -n 3 -t youtsuck < <( \
{ dirname_re="(^.*/)[^/]+$" ; [[ "${BASH_SOURCE[0]}" =~ $dirname_re ]] && cd "${BASH_REMATCH[1]}"
if [[ "${version}" =~ ^[hQ] ]] ; then
lsquic/http_client -o version=${version} -o delayed_acks=${delayed_acks} -o cc_algo=1 -o pace_packets=0 -0 /tmp/www.youtube.com,${version}.quic -4 \
-s www.youtube.com -p '/watch?v='"${id}"
else
https_proxy="${https_proxy}" wget --no-config --no-cookies --no-dns-cache --no-hsts --no-check-certificate --connect-timeout=10 --read-timeout=30 -q -t1 -O- -o /dev/null -4 \
'https://www.youtube.com/watch?v='"${id}"
fi ; } | \
tr , \\n | \
grep -E -s -o '"s=[^ \]+|https://[^. ]+\.googlevideo\.com/videoplayback[^ "]+|L":"/[^ "]+/base\.js' )
if [[ "${version}" == "h3" ]] ; then
if [[ ${#youtsuck[0]} -eq 0 ]] ; then
echo -n > /tmp/www.youtube.com,h3.quic ; fi ; fi
js=
signature=
url=
for i in "${youtsuck[@]}" ; do
[[ ${#js} -ne 0 ]] && [[ ${#signature} -ne 0 ]] && [[ ${#url} -ne 0 ]] && break
case "${i:0:3}" in
'L":' ) [[ ${#js} -eq 0 ]] && js="${i:4}" ; continue ;;
'htt' ) [[ ${#url} -eq 0 ]] && url="${i}" ; continue ;;
'"s=' ) [[ ${#signature} -eq 0 ]] && signature="${i:3}" ;;
esac ; done
if [[ ${#signature} -eq 0 ]] ; then
if [[ ${#url} -ne 0 ]] ; then
echo "${url//\\u0026/&}"
exit
fi
else
[[ ${#url} -ne 0 ]] && [[ ${#js} -ne 0 ]] || exit
youtsuck[0]="${signature//%3D/=}"
youtsuck[1]="${url//%3F/?}"
youtsuck[1]="${youtsuck[1]//%3D/=}"
youtsuck[1]="${youtsuck[1]//%25/%}"
youtsuck[1]="${youtsuck[1]//%26/&}"
json="$(echo -n "http://www.youtube.com${js}"|md5sum)"
json="${json:0:32}"
md5re="^[0-9a-fA-F]{32}$"
[[ "${json}" =~ $md5re ]] || exit
json="/tmp/${json}.json"
if [[ -f "${json}" ]] ; then
read instructions < "${json}" || exit
else
instructions=
version=
if [[ ${#https_proxy} -eq 0 && -x lsquic/http_client && $(($RANDOM % 2)) -eq 0 ]] ; then
if (( RANDOM % 2 )) ; then version=h3 ; delayed_acks=0 ; else version=h3-29 ; delayed_acks=0 ; fi ; fi
readarray -t js < <( \
{ dirname_re="(^.*/)[^/]+$" ; [[ "${BASH_SOURCE[0]}" =~ $dirname_re ]] && cd "${BASH_REMATCH[1]}"
if [[ "${version}" =~ ^[hQ] ]] ; then
lsquic/http_client -o version=${version} -o delayed_acks=${delayed_acks} -o cc_algo=1 -o pace_packets=0 -0 /tmp/www.youtube.com,${version}.quic -$((RANDOM%2*2+4)) \
-s www.youtube.com -p "${js}"
else
[[ ${#https_proxy} -ne 0 ]] && p=-4 || p=-$((RANDOM%2*2+4))
https_proxy="${https_proxy}" wget --no-config --no-cookies --no-dns-cache --no-hsts --no-check-certificate --connect-timeout=10 --read-timeout=30 -q -t1 -O- -o /dev/null $p \
'https://www.youtube.com'"${js}"
fi ; } | \
grep -P -s -o '(?<=^|[^$a-z_A-Z0-9])"?[$a-z_A-Z][$a-z_A-Z0-9]"?:function[^}]+?(?:%[a-zA-Z]\.length|splice|reverse)|(?<=\.split\(""\);)[^}]+?(?=return [a-zA-Z]\.join\(""\))|\b(?:sts|signatureTimestamp)[:=][0-9]{5}\b|\b[0-9]{5}[^0-9]+?signature_cipher_killswitch' )
if [[ "${version}" == "h3" ]] ; then
if [[ ${#js[0]} -eq 0 ]] ; then
echo -n > /tmp/www.youtube.com,h3.quic ; fi ; fi
sts=
splicer=
swapper=
reverser=
decoder=
for i in "${js[@]}" ; do
i="${i//\"}"
if [[ "${i:0:3}" == sts ]] ; then sts="${i:4}"
elif [[ "${i:0:18}" == signatureTimestamp ]] ; then sts="${i:19}"
elif [[ "${i:2:1}" =~ [.[] ]] ; then decoder="${i}"
elif [[ "${i:3:1}" =~ [.[] ]] ; then decoder="${i:1}"
elif [[ "${i}" =~ splice ]] ; then splicer="${i:0:2}"
elif [[ "${i}" =~ length ]] ; then swapper="${i:0:2}"
elif [[ "${i}" =~ reverse ]] ; then reverser="${i:0:2}"
elif [[ "${i}" =~ signature_cipher_killswitch ]] ; then sts="${i:0:5}" ; fi
done
if [[ ${#sts} -ne 0 ]] ; then
if [[ ${#splicer} -ne 0 ]] ; then
if [[ ${#swapper} -ne 0 ]] ; then
if [[ ${#reverser} -ne 0 ]] ; then
if [[ ${#decoder} -ne 0 ]] ; then
decoder="${decoder//([a-zA-Z],/ }"
decoder="${decoder//);/ }"
set -f ; decoder=($decoder)
N=${#decoder[@]}
instructions="[[\"sts\",${sts}]"
for (( i = 0 ; i < N ; ++i )) ; do
case "${decoder[i]: -2:2}" in
"${splicer}" ) instructions+=",[\"splice\",${decoder[i+1]}]" ;;
"${swapper}" ) instructions+=",[\"swap\",${decoder[i+1]}]" ;;
"${reverser}" ) instructions+=",[\"reverse\"]" ;;
esac ; done
instructions+="]"
fi ; fi ; fi ; fi ; fi
[[ ${#instructions} -ne 0 ]] || exit
set -C ; echo "${instructions}" > "${json}" || exit
fi
json_cipher "${instructions}"
echo "${youtsuck[1]}&sig=${youtsuck[0]}"
exit
fi ; fi
