# YoutSuck

YouTube downloader in Bash

Default mode is `youtubei/v1/player "ADUNIT"`

> bash youtsuck.sh https://youtu.be/jNQXAC9IVRw

Put `#` after the URL for `youtubei/v1/player "EMBED"`

> bash youtsuck.sh https://youtu.be/jNQXAC9IVRw#

Put `?` after the URL for HTML scraping.

> bash youtsuck.sh https://youtu.be/jNQXAC9IVRw?

Requires: wget tr grep md5sum
